extends Area2D

var destination = Vector2()
var isAlive = true
var isFlipped = false
var motion = Vector2()
export var roaming_size = 100
export var speed = 50

func _ready():
	destination = position+Vector2(-roaming_size, 0)

func _physics_process(delta):
	if (isAlive):
		update_motion(delta)
	

func _process(delta):
	if (isAlive):
		$AnimationPlayer.play("walk")
	else:
		$AnimationPlayer.play("die")

func update_motion(delta):
	if (isFlipped):
		position.x += speed*delta
		if (position.x >= destination.x):
			switch_destination()
	else:
		position.x -= speed*delta
		if (position.x <= destination.x):
			switch_destination()
			
	
func switch_destination():
	if (isFlipped):
		destination = destination+Vector2(-2*roaming_size,0)
	else:
		destination = destination+Vector2(2*roaming_size,0)
		
	isFlipped = !isFlipped
	$Sprite.flip_h = isFlipped


func _on_Porcupine_body_entered(body):
	Global.Player.hurt()


func _on_Porcupine_area_entered(area):
	isAlive = false
	
