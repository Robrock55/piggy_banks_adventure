extends Area2D

func _on_Coin_body_entered(body):
	if (Global.GameState.coins < Config.MAX_COINS):
		$AnimationPlayer.play("die")
		Global.GameState.add_coin()
		
