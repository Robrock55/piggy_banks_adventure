extends Area2D

var life
var speed

func _ready():
	position = Vector2(0,0)

func _physics_process(delta):
	position += Vector2(speed * delta, 0)

func _process(delta):
	life -= delta
	if (life <= 0):
		queue_free()

func _on_BBQBullet_body_entered(body):
	Global.Player.hurt()
	queue_free()
