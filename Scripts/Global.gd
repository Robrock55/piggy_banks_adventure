extends Node

var CurrentCamera
var GameState
var GUI
var Player

func _ready():
	assign_default_controls()

func _process(delta):
	if (Input.is_action_pressed("ui_cancel")):
		get_tree().quit()
		
func assign_default_controls():
	var controls = ["fire", "left", "right", "jump", "crouch"]
	var inputs_list
	for control in controls:
		inputs_list = InputMap.get_action_list("default_"+control)
		for input in inputs_list:
			InputMap.action_add_event("current_"+control, input)
