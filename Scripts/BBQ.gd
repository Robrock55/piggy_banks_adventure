extends Area2D

export var cooldown = 3
var currentCooldown = 0
export var bulletLifetime = 1
export var bulletSpeed = 500
var BBQ_bullet_scene = load("res://Scenes/BBQBullet.tscn")

func _process(delta):
	currentCooldown -= delta
	if (currentCooldown <= 0):
		shoot()
		currentCooldown = cooldown

func _ready():
	currentCooldown = cooldown

func _on_BBQ_body_entered(body):
	Global.Player.hurt()
	
func create_bullet():
	var scene_instance = BBQ_bullet_scene.instance()
	scene_instance.life = bulletLifetime
	scene_instance.speed = -bulletSpeed
	add_child(scene_instance)
	
func shoot():
	$AnimationPlayer.play("shoot")
