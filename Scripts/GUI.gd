extends CanvasLayer

func _ready():
	Global.GUI = self
	
func _on_RetryButton_pressed():
	get_tree().reload_current_scene()
	
func set_coins_number(coins):
	$LifeInfo/Counter.text = str(coins)

func show_end_game_screen():
	$VictoryMusic.play()
	$LifeInfo.hide()
	$VictoryScreen.show()
		
func show_retry_screen():
	$RetryButton.disabled = false
	$RetryButton.show()
