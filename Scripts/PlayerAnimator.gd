extends AnimationPlayer

const MODULATION = 0.05

func _ready():
	$InvincibilityTween.interpolate_property($"../Sprite", "visible", true, false, 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$InvincibilityTween.start()
	$InvincibilityTween.set_active(false)

func run_invincibililty_tween():
	$InvincibilityTween.set_active(true)

func update_animation(motion, isHurt, isOnFloor, isCrouch, isAlive, isInvincible, isPlayable):
	if (!isPlayable):
		play("idle")
		return
	
	# Flip
	if motion.x > 0:
		$"../Sprite".flip_h = false
	elif motion.x < 0:
		$"../Sprite".flip_h = true
	
	if (isAlive):
		if isHurt:
			play("hurt")
		elif isCrouch:
			play("crouch")
		elif motion.y < 0:
			play("jump")
		elif motion.y > 0 && !isOnFloor:
			play("fall")
		elif motion.x > 0:
			play("run")
		elif motion.x < 0:
			play("run")
		else: 
			play("idle")
		
		if (!isInvincible && $InvincibilityTween.is_active()):
			$InvincibilityTween.set_active(false)
			$"../Sprite".visible = true
			
		$"../Sprite".self_modulate = Color(1, 1 - MODULATION*(Global.GameState.coins-3) , 1 - MODULATION*(Global.GameState.coins-3))
	else:
		$InvincibilityTween.set_active(false)
		$"../Sprite".visible = true
		play("dead")
		