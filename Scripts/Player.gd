extends KinematicBody2D

var bullet_scene = load("res://Scenes/Bullet.tscn")
var can_shoot = true
var invincibility = 0
var isAlive = true
var isCrouch = false
var isHurt = false
var isPlayable = true
var motion = Vector2()

func _physics_process(delta):
	update_motion(delta)

func _process(delta):
	update_invincibity(delta)
	update_animation()
	
func _ready():
	Global.Player = self
	
func crouch():
	if Input.is_action_pressed("current_crouch") && is_on_floor():
		isCrouch = true
	elif Input.is_action_just_released("current_crouch"):
		isCrouch = false
	
func fall(delta):
	if is_on_floor() || is_on_ceiling():
		motion.y = 0
	if (!is_on_floor()):
		motion.y += Config.GRAVITY * delta
		
	motion.y = clamp(motion.y, -Config.JUMP_SPEED, Config.JUMP_SPEED)

func hurt():
	if (invincibility <= 0):
		isHurt = true
		$HurtSound.play()
		Global.GameState.remove_coin()
		motion.y = -Config.HURT_Y_SPEED
		motion.x = Config.HURT_X_SPEED
		move_and_slide(Vector2(0,-1))
		set_invincibility(true)

func jump():
	if (isPlayable):
		if Input.is_action_pressed("current_jump") && is_on_floor():
			motion.y = -Config.JUMP_SPEED+(Config.JUMP_SPEED_MALUS*Global.GameState.coins)
			$JumpSound.play()

func kill():
	isAlive = false
	$CollisionShape2D.shape = null

func run():
	if (isPlayable):
		if Input.is_action_pressed("current_right") && !Input.is_action_pressed("current_left"):
			motion.x = Config.RUNNING_SPEED-(Config.RUNNING_SPEED_MALUS*Global.GameState.coins)
		elif Input.is_action_pressed("current_left") && !Input.is_action_pressed("current_right"):
			motion.x = -Config.RUNNING_SPEED+(Config.RUNNING_SPEED_MALUS*Global.GameState.coins)
		else: 
			motion.x = 0
		
func set_invincibility(status):
	if (status):
		invincibility = Config.INVINCIBILITY_DURATION
		$CollisionShape2D.disabled = true
		$AnimationPlayer.run_invincibililty_tween()
	else:
		$CollisionShape2D.disabled = false
		
func shoot():
	if (isPlayable):
		if (Input.is_action_just_pressed("current_fire") && Global.GameState.coins > 1):
			can_shoot = false
			var scene_instance = bullet_scene.instance()
			scene_instance.create_bullet(self, $Sprite.flip_h)
			scene_instance.position.x = position.x
			scene_instance.position.y = position.y
			get_tree().get_root().add_child(scene_instance)
			scene_instance.shoot()
			Global.GameState.remove_coin()
		elif (Input.is_action_just_released("current_fire")):
			can_shoot = true

func update_animation():
	$AnimationPlayer.update_animation(motion, isHurt, is_on_floor(), isCrouch, isAlive, invincibility > 0, isPlayable)
	
func update_invincibity(delta):
	if (invincibility > 0):
		invincibility -= delta
	else:
		set_invincibility(false)
		invincibility = 0
	
func update_motion(delta):
	if (isAlive):
		if (isHurt && is_on_floor()):
			isHurt = false
		else:
			run()
			fall(delta)
			jump()
			crouch()
			
		# DEADZONES
		if (motion.x < Config.MOTION_DEADZONE && motion.x > -Config.MOTION_DEADZONE):
			motion.x = 0
			
		if (motion.y < Config.MOTION_DEADZONE && motion.y > -Config.MOTION_DEADZONE):
			motion.y = 0
			
		if (isCrouch):
			motion.x = 0
		
		move_and_slide(motion, Config.UP)
		
		shoot()
			
		# if (position.y > ProjectSettings.get("display/window/size/height") + $Sprite.texture.get_height()):
		if (position.y > ProjectSettings.get("display/window/size/height")*1.5):
			Global.GameState.end_game()
	else:
		move_and_slide(Vector2(0,-Config.DYING_SPEED), Config.UP)