extends Node

var coins
var running

func _ready():
	running = true
	coins = Config.INIT_COINS
	Global.GameState = self
	Global.GUI.set_coins_number(coins)

func _on_EndGame_body_entered(body):
	complete_game()

func _process(delta):
	if (coins <= 0):
		end_game()

func add_coin():
	coins += 1
	Global.GUI.set_coins_number(coins)
	
func complete_game():
	$EndGame.queue_free()
	Global.Player.isPlayable = false
	$AudioStreamPlayer.stop()
	Global.GUI.show_end_game_screen()

func end_game():
	Global.Player.kill()
	Global.CurrentCamera.current = false
	Global.GUI.show_retry_screen()

func remove_coin():
	if (coins > 0):
		coins -= 1
		Global.GUI.set_coins_number(coins)


