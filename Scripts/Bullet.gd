extends Area2D

const BULLET_INIT_SPEED = 600
const BULLET_ACCELLERATION = 50

var isFlipped
var soundPosition
var player_ref
var speed

func _physics_process(delta):
	if (isFlipped):
		speed -= BULLET_ACCELLERATION
	else:
		speed += BULLET_ACCELLERATION
	position += Vector2(speed*delta, 0)
	
func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func create_bullet(player, flipped):
	player_ref = player
	isFlipped = flipped
	connect("is_dead", player, "death_coin")

func shoot():
	if (isFlipped):
		$Sprite.flip_h = true
		speed = -BULLET_INIT_SPEED
	else:
		speed = BULLET_INIT_SPEED
	$AnimationPlayer.play("shoot")
	$AudioStreamPlayer.play()


